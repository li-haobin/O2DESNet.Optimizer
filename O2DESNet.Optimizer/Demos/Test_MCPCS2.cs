﻿using O2DESNet.Optimizer;
using O2DESNet.Optimizer.Benchmarks.RankingAndSelection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos
{
    class Test_MCPCS2
    {
        static void Main(string[] args)
        {
            var sar1 = new OCBA();
            var sar2 = new MCPCS_2();

            int nDesigns = 20;
            var rns1 = new MonotoneDecreasingMeans(nDesigns: nDesigns, vPower: 1);
            var rns2 = new MonotoneDecreasingMeans(nDesigns: nDesigns, vPower: 1);
            var rns3 = new MonotoneDecreasingMeans(nDesigns: nDesigns, vPower: 1);
            //var rns1 = new SlippageConfiguration(nDesigns, 2);
            //var rns2 = new SlippageConfiguration(nDesigns, 2);
            //var rns3 = new SlippageConfiguration(nDesigns, 2);

            for (int i = 0; i < nDesigns; i++)
            {
                rns1.Evaluate(i, 2);
                rns2.Evaluate(i, 2);
                rns3.Evaluate(i, 2);
            }

            int budget = 30;

            Console.WriteLine("EA\tOCBA\tMC-PCS");
            for (int i = 0; i < 100; i++)
            {
                var alloc1 = sar1.Alloc(budget, rns1.Designs);
                foreach (var a in alloc1) rns1.Evaluate((int)a.Key[0], a.Value);

                var alloc2 = sar2.Alloc(budget, rns2.Designs);
                foreach (var a in alloc2) rns2.Evaluate((int)a.Key[0], a.Value);

                int n = budget / nDesigns;
                foreach (var a in alloc2) rns3.Evaluate((int)a.Key[0], n);

                Console.WriteLine("{0:F4}\t{1:F4}\t{2:F4}", rns3.PCS, rns1.PCS, rns2.PCS);//.Variance, rns2.Variance);
                //Console.ReadKey();
            }
        }
    }
}
