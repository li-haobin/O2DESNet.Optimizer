﻿using MathNet.Numerics.LinearAlgebra.Double;
using O2DESNet.Optimizer;
using O2DESNet.Optimizer.Benchmarks.RankingAndSelection;
using System;
using System.IO;

namespace Demos
{
    class Demo_ParetoSelection
    {
        static DenseVector[] TrueMeanVectors
        {
            get
            {
                return new DenseVector[] {
                    new double[] { 0.5, 5.5 },
                    new double[] { 1.9, 4.2 },
                    new double[] { 2.8, 3.3 },
                    new double[] { 3, 3 },
                    new double[] { 3.9, 2.1 },
                    new double[] { 4.3, 1.8 },
                    new double[] { 4.6, 1.5 },
                };
            }
        }

        static DenseVector[] TrueStdDevVectors
        {
            get
            {
                return new DenseVector[] {
                    new double[] { 2, 2 },
                    new double[] { 2, 2 },
                    new double[] { 2, 2 },
                    new double[] { 2, 2 },
                    new double[] { 2, 2 },
                    new double[] { 2, 2 },
                    new double[] { 2, 2 },
                };
            }
        }

        static void Main(string[] args)
        {
            var mocba = new MOCBA();

            // prepare for multi-replications file output
            var file_multi_replications = "output_multi_replications.csv";
            using (var sw = new StreamWriter(file_multi_replications)) { }

            // repeat meta-replications with different random seeds
            for (int seed = 0; seed < 1000; seed++)
            {
                // set-up benchmark problem
                var benchmark = new ParetoSelection(TrueMeanVectors, TrueStdDevVectors, seed);
                // evaluate with initial budget
                foreach (var design in benchmark.Designs) benchmark.Evaluate(design, budget: 5);

                // prepare for single-replication file output
                var file_single_replication = string.Format("output_single_replication_{0}.csv", seed);
                using (var sw = new StreamWriter(file_single_replication)) { }

                // iterates
                for (int i = 0; i < 400; i++)
                {
                    // evaluate based on MOCBA results
                    foreach (var alloc in mocba.Alloc(
                        budget: 10, // budget per iteration
                        solutions: benchmark.Designs
                        ))
                        benchmark.Evaluate(alloc);

                    // calculate PCS
                    //var pcs = benchmark.PCS(mcSampleSize: 1000);
                    var cs = benchmark.CorrectSelection ? 1 : 0;

                    // output to files
                    using (var sw = new StreamWriter(file_single_replication, true))
                    {
                        foreach (var design in benchmark.Designs) sw.Write("{0},", design.Observations.Count);
                        sw.WriteLine("{0}", cs); //pcs);
                    }
                    using (var sw = new StreamWriter(file_multi_replications, true)) sw.Write("{0},", cs); //pcs);

                    // console display
                    foreach (var design in benchmark.Designs) Console.Write("{0},", design.Observations.Count);
                    Console.WriteLine("\tPCS: {0}", cs); //pcs);
                }

                // prepare for multi-replications file output
                using (var sw = new StreamWriter(file_multi_replications, true)) sw.WriteLine();

            }
        }
    }
}