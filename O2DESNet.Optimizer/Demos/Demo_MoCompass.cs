﻿using MathNet.Numerics.LinearAlgebra.Double;
using O2DESNet.Optimizer;
using O2DESNet.Optimizer.Benchmarks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos
{
    class Demo_MoCompass
    {
        static void Main(string[] args)
        {
            Console.Write("Dimension for Decision Space (d): ");
            int d = Convert.ToInt32(Console.ReadLine());

            Console.Write("Discretization Levels (l): ");
            int l = Convert.ToInt32(Console.ReadLine());

            Console.Write("Batch Size (m): ");
            int m = Convert.ToInt32(Console.ReadLine());

            Console.Write("Number of Replications (r): ");
            int r = Convert.ToInt32(Console.ReadLine());
            
            var zdt = new ZDT4(nDecisions: d);
            var name = string.Format("{0}_d{1}_l{2}_m{3}_r{4}", zdt, d, l, m, r);

            var stats_vs_nIterations = new AverageRunningStats(r);
            var stats_vs_nEvalutions = new AverageRunningStats(r);

            for (int seed = 0; seed < r; seed++)
            {
                var rs = new Random(seed);
                var mc = new MoCompass(new ConvexSet(dimension: d, globalLb: 0, globalUb: l), seed: rs.Next(), initialDynamicBound: l);

                // initiate with a random decision from the decision space
                var init = Enumerable.Range(0, d).Select(i => Math.Round(rs.NextDouble() * l) / l).ToArray();
                mc.Enter(new StochasticSolution(init, zdt.Evaluate(init)));

                // iterate
                for (int nIterations = 0; nIterations < 5000; nIterations++)
                {
                    var sampledSet = mc.Sample(batchSize: m, decimals: 0).Select(decs => decs / l).ToList();
                    if (sampledSet.Count == 0) break;

                    foreach (var decs in sampledSet) mc.Enter(new StochasticSolution(decs, zdt.Evaluate(decs)));
                    var dhv = Pareto.DominatedHyperVolume(points: mc.ParetoSet.Select(s => s.Objectives), reference: new double[] { 1, 1 });

                    stats_vs_nIterations.Log(runId: seed, time: nIterations, value: dhv);
                    stats_vs_nEvalutions.Log(runId: seed, time: mc.AllSolutions.Count, value: dhv);

                    // display to console
                    Console.Clear();
                    Console.WriteLine(name + "\n");
                    Console.WriteLine("Seed\t#Iters\t#Evals\tDHV");
                    Console.WriteLine("{0}\t{1}\t{2}\t{3}", seed, nIterations, mc.AllSolutions.Count, dhv);
                }
            }

            // output to file
            using (var sw = new StreamWriter(name + "_byIters.csv"))
            {
                sw.WriteLine("#Iters,Avg.DHV");
                foreach (var t in stats_vs_nIterations.Output) sw.WriteLine("{0},{1}", t.Item1, t.Item2);
            }

            using (var sw = new StreamWriter(name + "_byEvalus.csv"))
            {
                sw.WriteLine("#Evals,Avg.DHV");
                foreach (var t in stats_vs_nEvalutions.Output) sw.WriteLine("{0},{1}", t.Item1, t.Item2);
            }
        }
    }
}
