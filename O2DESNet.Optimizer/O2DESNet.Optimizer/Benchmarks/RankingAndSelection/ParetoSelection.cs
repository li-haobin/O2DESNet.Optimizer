﻿using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra.Double;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace O2DESNet.Optimizer.Benchmarks.RankingAndSelection
{
    public class ParetoSelection
    {
        public int NObjectives { get; private set; }
        public StochasticSolution[] Designs { get; protected set; }
        public DenseVector[] TrueMeanVectors { get; protected set; }
        public DenseVector[] TrueStdDevVectors { get; protected set; }
        protected Random _defaultRS { get; private set; }
        protected Random[] _randoms { get; private set; }

        protected ParetoSelection(int nDesigns, int nObjectives, int seed)
        {
            Init(nDesigns, nObjectives, seed);
            TrueMeanVectors = new DenseVector[nDesigns];
            TrueStdDevVectors = new DenseVector[nDesigns];
        }

        public ParetoSelection(DenseVector[] trueMeansVectors, DenseVector[] trueStdDevVectors, int seed)
        {
            Init(trueMeansVectors.Length, trueMeansVectors.Min(v => v.Count), seed);
            TrueMeanVectors = trueMeansVectors.ToArray();
            TrueStdDevVectors = trueStdDevVectors.ToArray();
        }

        private void Init(int nDesigns, int nObjectives, int seed)
        {
            NObjectives = nObjectives;
            Designs = Enumerable.Range(0, nDesigns).Select(i => new StochasticSolution(new double[] { i })).ToArray();
            _defaultRS = new Random(seed);
            _randoms = Designs.Select(s => new Random(_defaultRS.Next())).ToArray();
        }

        public void Evaluate(int index, int budget)
        {
            for (int i = 0; i < budget; i++)
            {
                Designs[index].Evaluate(Enumerable.Range(0, NObjectives).Select(o => Normal.Sample(_randoms[index], TrueMeanVectors[index][o], TrueStdDevVectors[index][o])).ToArray());
            }
        }

        public void Evaluate(StochasticSolution design, int budget) { Evaluate((int)design.Decisions[0], budget); }

        public void Evaluate(KeyValuePair<DenseVector, int> alloc) { Evaluate((int)alloc.Key[0], alloc.Value); }

        /// <summary>
        /// Estimate PCS by Monte Carlo sampling
        /// </summary>
        /// <param name="mcSampleSize">Monte Carlo sample size</param>
        /// <returns></returns>
        public double PCS(int mcSampleSize = 1000, Random rs = null)
        {
            if (rs == null) rs = _defaultRS;
            return Pareto.PCS_MonteCarlo(Designs, rs, mcSampleSize);
        }

        public bool CorrectSelection
        {
            get
            {
                var indices = Enumerable.Range(0, Designs.Length);
                var trueParetoSet = new HashSet<int>(Pareto.GetParetoSet(indices, i => TrueMeanVectors[i]));
                var observedParetoSet = new HashSet<int>(Pareto.GetParetoSet(indices, i => Designs[i].Objectives));
                if (trueParetoSet.Count != observedParetoSet.Count) return false;
                foreach (var i in trueParetoSet) if (!observedParetoSet.Contains(i)) return false;
                foreach (var i in observedParetoSet) if (!trueParetoSet.Contains(i)) return false;
                return true;
            }
        }
    }
}
