﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.Statistics;
using MathNet.Numerics.Distributions;

namespace O2DESNet.Optimizer
{
    public class MCPCS_2 : SAR
    {
        private List<double[,]> MCSamples = new List<double[,]>();
        private Design[] Designs;

        public override Dictionary<DenseVector, int> Alloc(int budget, IEnumerable<StochasticSolution> solutions)
        {
            // Pre-allocation
            var pre_alloc = PreAlloc(ref budget, ref solutions);
            if (solutions.Count() < 1 || budget == 0) return pre_alloc;

            var mcSize = 1000;            
            int id = 0;
            Designs = solutions.Select(s => new Design
            {
                Id = id++,
                Mean = s.Observations.Select(o => o[0]).Mean(),
                Stddev = s.Observations.Select(o => o[0]).StandardDeviation(),
                NSamples = s.Observations.Count,
            }).ToArray();
            var min = Designs.Min(d => d.Mean);
            Designs = Designs.OrderBy(d => (d.Mean - min) / d.Stddev * Math.Sqrt(d.NSamples)).ToArray();
            var Rnds = Enumerable.Range(0, Designs.Length).Select(i => new Random(i)).ToArray();
            var decisions = solutions.Select(s => s.Decisions).ToArray();

            // MC Sampling            
            for (int k = 0; k < mcSize; k++)
            {
                MCSamples.Add(new double[Designs.Length, budget + 1]);
                Parallel.For(0, Designs.Length, i =>
                {
                    var design = Designs[i];
                    var deviation = Normal.Sample(Rnds[design.Id], 0, design.Stddev);
                    for (int n = 0; n <= budget; n++) MCSamples.Last()[i, n] = design.Mean + deviation / Math.Sqrt(design.NSamples + n);
                });
            }

            // Allocation
            Tuple<int[], double> globalBest = new Tuple<int[], double>(null, 0);
            Parallel.For(0, budget, a0 =>
            //for (int a0 = 0; a0 < Budget; a0++)
            {
                // fix the budget for the 1st design
                int[] alloc = new int[Designs.Length];
                alloc[0] = a0;
                var localBest = new Tuple<int[], double>(alloc, 0);

                // search for the allocation for the rest
                for (int i = 0; i < budget - a0; i++)
                {
                    var list = new List<Tuple<int[], double>>();
                    for (int j = 1; j < Designs.Length; j++)
                    {
                        var a = localBest.Item1.ToArray();
                        a[j]++;
                        list.Add(new Tuple<int[], double>(a, ObservedPCS(a)));
                    }
                    localBest = list.Aggregate((t1, t2) => t1.Item2 > t2.Item2 ? t1 : t2);
                    lock (Designs)
                    {
                        if (localBest.Item2 > globalBest.Item2)
                            globalBest = localBest;
                    }
                }
                //foreach (var a in localBest.Item1) Console.Write("{0} ", a);
                //Console.WriteLine(localBest.Item2);
            }
            );
            //foreach (var a in globalBest.Item1) Console.Write("{0} ", a);
            //Console.WriteLine(globalBest.Item2);
            var dict =  Enumerable.Range(0, Designs.Length).ToDictionary(i => decisions[i], i => globalBest.Item1[Designs[i].Id]);
            foreach (var a in pre_alloc) dict.Add(a.Key, a.Value);
            return dict;
        }

        // count correct selection
        private double ObservedPCS(int[] alloc)
        {
            int falseCount = 0;
            for (int j = 0; j < MCSamples.Count; j++)
                for (int i = 1; i < Designs.Length; i++)
                {
                    if (MCSamples[j][i, alloc[i]] < MCSamples[j][0, alloc[0]])
                    {
                        falseCount++;
                        break;
                    }
                }
            return 1 - 1.0 * falseCount / MCSamples.Count;
        }

        public class Design
        {
            public int Id { get; set; }
            public double Mean { get; set; }
            public double Stddev { get; set; }
            public int NSamples { get; set; }
        }
    }
}
