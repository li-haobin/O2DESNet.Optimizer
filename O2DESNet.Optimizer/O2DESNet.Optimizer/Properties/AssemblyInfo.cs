﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("O2DESNet.Optimizer")]
[assembly: AssemblyDescription("A framework for Object-Oriented Discrete Event Simulation")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Haobin Li, Giulia Pedrielli, Loo Hay Lee")]
[assembly: AssemblyProduct("O2DESNet.Optimizer")]
[assembly: AssemblyCopyright("Copyright © 2016 - 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("b7406337-f0c7-4dac-931a-8fb09134d12a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.2.1.0")]
[assembly: AssemblyFileVersion("1.2.1.0")]
